---
layout: default
title: Unified Achievement Hunting Rules - Old Changelog
permalink: /old_changelog
---

##  Old Changelog 

2019 Aug 2 - Minor changes to formatting and conversion to new site, and edited a few examples for accuracy/clarity

2019 JUL 15 - Added example to Allowed Rule 4.

+ Clarified Penalties for Cheating section.
+ Added exception links to Not Allowed Rule 3.

2019 JUN 29 - Added more in-doc reference links to rules.

2019 JUN 28 - Added clarifying text to Not Allowed Rule 3: Do not modify game code.

+ Added thanks to community members for their continued input.
+ Fixed typo.

2019 JUN 26 - Added feedback section & merged need help section.

2019 JUN 15 - Fixed typo in Allowed Rule 19. - Thanks, Sellyme!

2019 JUN 10 - Added Exophase to Penalties for Cheating partnership list.

2019 MAY 28 - Added in-doc reference links to several rules for easier navigation.

2019 MAY 26 - Added adoptive partners and fixing gramatical errors.

2019 JAN 17 - Rewrote Not Allowed Rule 7 to include in-game cheat/debug menus.

2019 JAN 04 - Added links to How to Appeal section.

2018 DEC 23 - Indexed Rules now all link to their respective descriptions. Added links to go back to Table of Contents.

2018 DEC 13 - Rewrote Not Allowed Rule 1 for clarity.

2018 NOV 12 - Added pencil emoji to Changelog header.

2018 OCT 07 - Added Allowed Rule 19: "Reading game files and game code is allowed."

2018 SEP 26 - First upload of rules.

